# Lab 7 statement

The objective of this lab is to illustrate Tree based classification and regression methods. Forest trees are introduced as a natural bagging method.  

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the questions` or `Exercise` cells._

See the notebooks in the [`8_Trees_Boosting`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/) folder


1. Firsts steps with classification trees [`N1_Classif_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N1_Classif_tree.ipynb)


2. Examples of regression trees [`N2_a_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_a_Regression_tree.ipynb) and cost complexity pruning methods [`N2_b_Cost_Complexity_Pruning_Regressor.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_b_Cost_Complexity_Pruning_Regressor.ipynb)


3.The 3 notebooks below illstrate the concept of bagging through the application of random forests
 [`N2_a_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_a_Regression_tree.ipynb)


4.  Implement your own version of EM for Gaussian model  and apply it to the same example used for Kmeans in a preceeding notebook. Compare with KMeans and interpret the results [`N2_a_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_a_Regression_tree.ipynb)



5. Example of EM application on the Iris data set [`N2_a_Regression_tree.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/8_Trees_Boosting/N2_a_Regression_tree.ipynb)

